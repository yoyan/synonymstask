﻿using SynonymsTask.Core.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SynonymsTask.Core.Context
{
    public class SynonymDbContext : DbContext
    {
        public SynonymDbContext() : base()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Synonym> Synonym { get; set; }

    }
}
