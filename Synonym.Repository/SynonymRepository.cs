﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SynonymsTask.Core.Data;
using SynonymsTask.Core.Context;

namespace SynonymsTask.Core.Repository
{
    public class SynonymRepository
    {
        /// <summary>
        /// Return all the synonyms from database formated in a list
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Synonym> GetList()
        {
            using (SynonymDbContext dbContext = new SynonymDbContext())
            {
                return dbContext.Synonym.ToList();
            }
        }

        /// <summary>
        /// Add a new synonym to the database
        /// </summary>
        /// <param name="icSynonym"></param>
        /// <returns></returns>
        public int AddSynonym(Synonym icSynonym)
        {
            // TODO : check if properties are not null
            using (SynonymDbContext dbContext = new SynonymDbContext())
            {
                dbContext.Entry(icSynonym).State = System.Data.Entity.EntityState.Added;
                dbContext.SaveChanges();
            }

            return icSynonym.iId;
        }

        /// <summary>
        /// Remove an existing synonym to the database by ID
        /// </summary>
        /// <param name="icSynonym"></param>
        /// <returns></returns>
        public void RemoveById(int iSynonymId)
        {
            using (SynonymDbContext dbContext = new SynonymDbContext())
            {
                Synonym icSynonymToDelete = dbContext.Synonym.Find(iSynonymId);
                dbContext.Entry(icSynonymToDelete).State = System.Data.Entity.EntityState.Deleted;
                dbContext.SaveChanges();
            }
        }

        /// <summary>
        /// Remove one or several synonym according to the term in input parameter
        /// </summary>
        /// <param name="icSynonym"></param>
        /// <returns></returns>
        public void RemoveByTerm(string sTerm)
        {
            using (SynonymDbContext dbContext = new SynonymDbContext())
            {
                List<Synonym> cSynonymsToDelete = dbContext.Synonym.Where(s => s.sTerm == sTerm).ToList();

                foreach (Synonym icSynonym in cSynonymsToDelete)
                {
                    dbContext.Entry(icSynonym).State = System.Data.Entity.EntityState.Deleted;
                }

                dbContext.SaveChanges();
            }
        }

        /// <summary>
        /// Remove all rows of the synonym table. Used for the development.
        /// </summary>
        public void RemoveAll()
        {
            using (SynonymDbContext dbContext = new SynonymDbContext())
            {
                dbContext.Database.ExecuteSqlCommand("DELETE FROM [dbo].[t_synonym]");
            }
        }
    }
}
