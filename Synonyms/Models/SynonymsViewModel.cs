﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SynonymsTask.Models
{

    public class SynonymListViewModel
    {
        public string sWord { get; set; }

        public List<SynonymViewModel> cWordAndSynonimsList { get; set; }
    }

    public class SynonymViewModel
    {
        public string sWord { get; set; }

        public List<string> cWordList { get; set; }
    }

    public class AddSynonymViewModel
    {
        public string sWord { get; set; }

        public string sSynonyms { get; set; }
    }
}