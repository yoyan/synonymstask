﻿var SynonymsTask = SynonymsTask || {};

SynonymsTask.Synonyme = (function () {

    var _options = {
        sEmptyListMessage: "",
    };

    var eNotification = {
        SUCCESS: 0,
        ERROR: 1,
        INFO: 2,
        WARNING: 3
    };

    // --------------------------------------------------------
    // ###1 Private methods
    // --------------------------------------------------------

    var _InitDOMEvents = function () {

        // Add a new Synonym
        $("#addSynonymButton").on("click", function () {

            var data = {
                sWord: $("#word").val().replace(/\s/g, '').toLowerCase(),
                sSynonyms: $("#synonyms").val().replace(/\s/g, '').toLowerCase()
            };

            addSynonym(data);
        });

        // delete an existing Term (one or multiple synonym)
        $(document).on("click", ".deleteSingleTerm", function (e) {
            debugger;
            var sTerm = $(e.target).attr("data-term");
            deleteSingleTerm(sTerm);
        })

        // Remove all synonym
        $("#resetDbButton").on("click", function () { removeAllSynonyms(); });

        // Click on aff button on enter key press
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $("#addSynonymButton").click();
            }
        });

    };

    /*
    * Function to add a new word and associated synonyms
    *   --> Check input parameters
    *   --> Ajax call
    *   --> Display the new content
    */
    var addSynonym = function (icTermModel) {

        // Check integrity of icTermModel object
        if (typeof (icTermModel) != "undefined" && icTermModel != null) {
            // sWord property
            if (icTermModel.hasOwnProperty("sWord")) {
                if (typeof (icTermModel.sWord) != "string") {
                    console.error("The property sWord of the object icTermModel must be a string");
                    return;
                }
            } else {
                console.error("The object icTermModel missed the property sWord");
                return;
            }

            // sSynonyms property
            if (icTermModel.hasOwnProperty("sSynonyms")) {
                if (typeof (icTermModel.sSynonyms) != "string") {
                    console.error("The property sSynonyms of the object icTermModel must be a string");
                    return;
                }
            } else {
                console.error("The object icTermModel missed the property sSynonyms");
                return;
            }
        } else {
            console.error("The object icTermModel mst be defined");
            return;
        }


        if (checkInputs(icTermModel)) {

            $.post("/Home/AddSynonym", icTermModel)
                .done(function (result) {
                    refresh("synonym-container", result);
                    $("#emptyListInfo").html("");
                    resetForm();
                    displayNotification("The word and synonyms have been added.", eNotification.SUCCESS);
                });
        }
    };

    /*
    * Check the validity of the inputs Word ans synonym list
    */
    var checkInputs = function (icTermModel) {

        // -------------------------------------------
        // Word verifications
        // -------------------------------------------

        // Empty
        if (icTermModel.sWord == "") {
            displayNotification("The word can't be empty.", eNotification.ERROR);
            return false;
        }

        // only alphabet or "-"
        if (!/^[a-z\-]+$/i.test(icTermModel.sWord)) {
            displayNotification("The word can only contain letters or being a composed word like \"portable-computer\".", eNotification.ERROR);
            return false;
        }

        // not 2 following "-"
        if (/(-)(?=\1)/g.test(icTermModel.sWord)) {
            displayNotification("You can't add 2 successives \"-\" in a word.", eNotification.ERROR);
            return false;
        }

        // can't start with a "-"
        if (/^-/.test(icTermModel.sWord)) {
            displayNotification("You can't start a word with a \"-\".", eNotification.ERROR);
            return false;
        }

        // can't end with a "-"
        if (/-$/.test(icTermModel.sWord)) {
            displayNotification("You can't end a word with a \"-\".", eNotification.ERROR);
            return false;
        }

        // -------------------------------------------
        // Synonym list verifications
        // -------------------------------------------

        // Empty
        if (icTermModel.sSynonyms == "") {
            displayNotification("The list of synonym can't be empty.", eNotification.ERROR);
            return false;
        }

        // Only alpha or "-" or ","
        if (!/^[a-z\-,]+$/i.test(icTermModel.sSynonyms)) {
            displayNotification("The synonym list can only be composed of word whch contain letters or being a composed word like \"portable-computer\".", eNotification.ERROR);
            return false;
        }

        // not 2 following "-"
        if (/(-)(?=\1)/g.test(icTermModel.sSynonyms)) {
            displayNotification("You can't add 2 successives \"-\" in the synonym list.", eNotification.ERROR);
            return false;
        }

        // not 2 following ","
        if (/(,)(?=\1)/g.test(icTermModel.sSynonyms)) {
            displayNotification("You can't add 2 successives \",\" in the synonym list.", eNotification.ERROR);
            return false;
        }

        // can't start with a ","
        if (/^,/.test(icTermModel.sSynonyms)) {
            displayNotification("The synonym list can't end with a \",\".", eNotification.ERROR);
            return false;
        }

        // can't end with a ","
        if (/,$/.test(icTermModel.sSynonyms)) {
            displayNotification("The synonym list can't end with a \",\".", eNotification.ERROR);
            return false;
        }

        // can't start with a "-"
        if (/^-/.test(icTermModel.sSynonyms)) {
            displayNotification("You can't start a word in the synonym list with a \"-\".", eNotification.ERROR);
            return false;
        }

        // can't end with a "-"
        if (/-$/.test(icTermModel.sSynonyms)) {
            displayNotification("You can't end a word in the synonym list with a \"-\".", eNotification.ERROR);
            return false;
        }

        // can't be identical to the word
        var regexSameWord = new RegExp("\s" + icTermModel.sWord + "\s");
        var regexSameWordStart = new RegExp("^" + icTermModel.sWord + ",");
        var regexSameWordEnd = new RegExp("," + icTermModel.sWord + "$");
        var regexSameWordMiddle = new RegExp("," + icTermModel.sWord + ",");

        if (icTermModel.sSynonyms == icTermModel.sWord
            || regexSameWordStart.test(icTermModel.sSynonyms)
            || regexSameWordEnd.test(icTermModel.sSynonyms)
            || regexSameWordMiddle.test(icTermModel.sSynonyms)) {
            displayNotification("You can't add the word itself to the synonym list.", eNotification.ERROR);
            return false;
        }

        return true;
    };

    /*
    * Refresh the list of 2-way synonyms
    *   --> Construct the new dom elements to display thank to a dictionary of terms and their synonyms
    */
    var refresh = function (containerId, cTermModel) {

        // container where we will display the synonym blocks
        var container = $("#" + containerId);
        container.html("");

        $(cTermModel).each(function (key, icTermModel) {
            // Get the list of synonyms in a string separated by coma
            var synonyms = icTermModel.cSynonym;
            var stringSynonyms = synonyms.join(",");

            // Creation of the DOM elements to display the new datas
            var htmlWordGroupWrapper = $("<div>").addClass("col-md-3");
            var htmlWordGroup = $("<div>").addClass("word-group").attr("data-term", icTermModel.sLabel);
            var htmlWord = $("<h4>").html(icTermModel.sLabel);
            var htmlSynonymContainer = $("<ul>");
            var htmlSynonymsList = $("<li>").html(stringSynonyms);

            // Specific part for synonyme created by user (in db)
            if (icTermModel.bFromUser) {

                var htmlInfoUser = $("<span>").addClass("deduction-info").html("(created by user)");
                htmlWord.append(htmlInfoUser);

                var htmlTrashIcone = $("<span>")
                    .addClass("glyphicon glyphicon-trash pull-right deleteSingleTerm")
                    .attr("data-term-id", icTermModel.iId)
                    .attr("data-term", icTermModel.sLabel);

                htmlWord.append(htmlTrashIcone);

                htmlWordGroup.attr("data-term-id", icTermModel.iId)
                htmlWordGroupWrapper.addClass("userTerm");
            } else {
                var htmlInfoUser = $("<span>").addClass("deduction-info").html("(deducted from 2-way)");
                htmlWord.append(htmlInfoUser);
            }

            // Construct the DOM elements hierarchy
            htmlSynonymContainer.append(htmlSynonymsList);
            htmlWordGroup.append(htmlWord);
            htmlWordGroup.append(htmlSynonymContainer);
            htmlWordGroupWrapper.append(htmlWordGroup);
            // Append to the container
            container.append(htmlWordGroupWrapper);
        });
    };

    var displayNotification = function (text, type) {

        if (typeof (type) == "undefined") {
            type = eNotification.SUCCESS;
        }

        var sNotificationSelector = ".notification.alert.alert-info";

        switch (type) {
            case eNotification.SUCCESS:
                sNotificationSelector = ".notification.alert.alert-success";
                break;
            case eNotification.ERROR:
                sNotificationSelector = ".notification.alert.alert-danger";
                break;
            case eNotification.INFO:
                sNotificationSelector = ".notification.alert.alert-info";
                break;
            case eNotification.WARNING:
                sNotificationSelector = ".notification.alert.alert-warning";
                break;
        }
        $("[class*='notification alert alert-']").stop(true, true);

        $(sNotificationSelector).html(text);
        $(sNotificationSelector).show();
        $(sNotificationSelector).fadeOut(5000);
    };

    var resetForm = function () {
        $("#word").val("");
        $("#synonyms").val("");
    }; 

    /*
    * Delete one or several synonyme depending of the term
    */
    var deleteSingleTerm = function (sTerm) {

        var bDelete = confirm("Are you sure to delete this Word and its synonyms ?");
        if (bDelete) {

            $.post("/Home/DeleteSynonym", { sTerm: sTerm })
                .done(function (result) {
                    refresh("synonym-container", result);
                    if ($("#synonym-container").children().length == 0) {
                        $("#emptyListInfo").html(_options.sEmptyListMessage);
                    }
                    displayNotification("The word and its synonyms have been deleted.", eNotification.SUCCESS);
                });

        }
    };

    /*
    * Function to remove all synonyms from the database
    */
    var removeAllSynonyms = function () {
        var bDelete = confirm("Are you sure to delete all words and their synonyms ?");
        if (bDelete) {
            $.post("/Home/RemoveAllSynonyms")
                .done(function (data) {
                    var container = $("#synonym-container");
                    container.html("");
                    $("#emptyListInfo").html(_options.sEmptyListMessage);
                    resetForm();
                    displayNotification("All words and synonyms have been deleted.", eNotification.SUCCESS);
                });
        }
    };



    // --------------------------------------------------------
    // ####2 Public methods
    // --------------------------------------------------------
    return {



        Init: function (options) {

            _InitDOMEvents();
            _options = options;
        },

        DisplayNotification: function (text, type) {
            this.displayNotification(text, type);
        }
    }
})();