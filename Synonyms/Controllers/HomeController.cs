﻿using SynonymsTask.Models;
using SynonymsTask.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SynonymsTask.Core.Data;

namespace SynonymsTask.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Injected service to deal with synonym
        /// </summary>
        private readonly ISynonymService _icService;

        // Injection of the service trough the constructor and Unity DI library
        public HomeController(ISynonymService icService)
        {
            _icService = icService;
        }

        public ActionResult Index()
        {
            var model = new List<TermModel>();

            try
            {
                // Get the synonym dictionnary formatted
                model = _icService.GetTermModelList();
            }
            catch (Exception)
            {
                // TODO: log
                throw;
            }

            return View(model);
        }

        /// <summary>
        /// Add a new synonym (Term + list of synonym)
        /// </summary>
        /// <param name="mSynonym"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddSynonym(AddSynonymViewModel mSynonym)
        {
            // Will contain the new updated list to display to the user
            var cRefreshedSynonyms = new List<TermModel>();

            // Distinct on the synonyme string
            IEnumerable<string> cSplitedSynonyme = mSynonym.sSynonyms.Split(',');
            mSynonym.sSynonyms = String.Join(",", cSplitedSynonyme.Distinct().ToArray());

            try
            {
                // Add the new synonyme to the db
                _icService.Add(mSynonym.sWord, mSynonym.sSynonyms);
                cRefreshedSynonyms = _icService.GetTermModelList();
            }
            catch (Exception)
            {
                // TODO: Log
                throw;
            }

            return Json(cRefreshedSynonyms);
        }

        /// <summary>
        /// Delete an existing synonym by Id
        /// </summary>
        /// <param name="iSynonymId"></param>
        /// <returns></returns>
        public JsonResult DeleteSynonym(string sTerm)
        {
            // Will contain the new updated list to display to the user
            var cRefreshedSynonyms = new List<TermModel>();

            try
            {
                // Delete the synonym by Id from the DB
                _icService.RemoveByTerm(sTerm);
                cRefreshedSynonyms = _icService.GetTermModelList();
            }
            catch (Exception)
            {
                // TODO: Log
                throw;
            }

            return Json(cRefreshedSynonyms);

        }
        /// <summary>
        /// Remove all the synonims
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult RemoveAllSynonyms()
        {
            try
            {
                _icService.RemoveAll();
            }
            catch (Exception)
            {
                // TODO: Log
                throw;
            }

            return Json(true);
        }
    }
}