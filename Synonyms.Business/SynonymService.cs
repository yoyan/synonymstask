﻿using SynonymsTask.Core.Context;
using SynonymsTask.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SynonymsTask.Core.Data;
using SynonymsTask.Core.Repository;

namespace SynonymsTask.Core.Services
{
    /// <summary>
    /// A service which implements method to deal with Synonyms
    /// </summary>
    public class SynonymService : ISynonymService
    {
        /// <summary>
        /// Get a list of Terms and associated synonyms
        /// </summary>
        /// <returns></returns>
        public List<TermModel> GetTermModelList()
        {
            // Output List of Term Model
            var result = new List<TermModel>();

            // Get the data from database
            SynonymRepository repo = new SynonymRepository();
            IEnumerable<Synonym> cSynonyms = repo.GetList().OrderBy(s => s.sTerm);

            // Construct the output list
            foreach (Synonym icSynonym in cSynonyms)
            {
                // Transform the string of synonyms in a list of string
                List<string> cTermSynonyms = icSynonym.sSynonyms.Split(',').ToList();

                // -------------------------------------------
                // Update the output dictionary from the term
                // -------------------------------------------
                if (!result.Any(t => t.sLabel == icSynonym.sTerm))
                {
                    // New term and its synonyms
                    result.Add(new TermModel()
                    {
                        iId = icSynonym.iId,
                        sLabel = icSynonym.sTerm,
                        cSynonym = cTermSynonyms,
                        bFromUser = true
                    });
                }
                else
                {
                    // We update the fact that the row is not deducted but from the db
                    result.Single(t => t.sLabel == icSynonym.sTerm).bFromUser = true;
                    result.Single(t => t.sLabel == icSynonym.sTerm).iId = icSynonym.iId;

                    // Existing term => Update with new synonyms if there is
                    cTermSynonyms.ForEach(t => {
                        if (!result.Single( x => x.sLabel == icSynonym.sTerm).cSynonym.Contains(t))
                        {
                            result.Single(x => x.sLabel == icSynonym.sTerm).cSynonym.Add(t);
                        }
                    });
                }

                // -------------------------------------------------------------------
                // Update the output dictionary by deduction (from the 2 way synonym)
                // -------------------------------------------------------------------
                foreach (string sTerm in cTermSynonyms)
                {
                    if (!result.Any(t => t.sLabel == sTerm))
                    {
                        // New term and its synonyms
                        result.Add(new TermModel()
                        {
                            sLabel = sTerm,
                            cSynonym = new List<string>() { icSynonym.sTerm },
                            bFromUser = false
                        });
                    }
                    else
                    {
                        // Existing term => Update with new synonyms if there is
                        if (!result.Single(t => t.sLabel== sTerm).cSynonym.Contains(icSynonym.sTerm))
                        {
                            result.Single(t => t.sLabel== sTerm).cSynonym.Add(icSynonym.sTerm);
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Add a new Synonym
        /// </summary>
        /// <param name="sTerm"></param>
        /// <param name="sSynonyms"></param>
        public void Add(string sTerm, string sSynonyms)
        {
            SynonymRepository repo = new SynonymRepository();
            repo.AddSynonym(new Synonym() {
                sTerm = sTerm,
                sSynonyms = sSynonyms
            });
        }

        /// <summary>
        /// Remove an existing synonym by Id
        /// </summary>
        /// <param name="iSynonymId">Id of the synonym to delete</param>
        public void RemoveById(int iSynonymId)
        {
            SynonymRepository repo = new SynonymRepository();
            repo.RemoveById(iSynonymId);
        }

        /// <summary>
        /// Remove one or several synonyme according to the term in input parameter
        /// </summary>
        /// <param name="sTerm"></param>
        public void RemoveByTerm(string sTerm)
        {
            SynonymRepository repo = new SynonymRepository();
            repo.RemoveByTerm(sTerm);
        }

        /// <summary>
        /// Delete all the synonyms
        /// </summary>
        public void RemoveAll()
        {
            SynonymRepository repo = new SynonymRepository();
            repo.RemoveAll();
        }
    }
}
