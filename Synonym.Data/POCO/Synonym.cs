﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SynonymsTask.Core.Data
{
    [Table("t_synonym")]
    public class Synonym
    {
        [Key]
        [Column("id")]
        public int iId { get; set; }

        [Column("term")]
        public string sTerm { get; set; }

        [Column("synonyms")]
        public string sSynonyms { get; set; }
    }
}
