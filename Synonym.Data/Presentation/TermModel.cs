﻿using System;
using System.Collections.Generic;

namespace SynonymsTask.Core.Data
{
    /// <summary>
    /// Model used to organize data for the presentation layer
    /// </summary>
    public class TermModel
    {
        public int iId { get; set; }

        public string sLabel { get; set; }

        /// <summary>
        /// If true, it means that the Synonym is not deducted from other but created by a user and present in DB
        /// </summary>
        public bool bFromUser { get; set; }

        /// <summary>
        /// List of synonyms associated to the term
        /// </summary>
        public List<string> cSynonym { get; set; }

        public override string ToString()
        {
            return String.Join(",", cSynonym.ToArray());
        }
    }
}
