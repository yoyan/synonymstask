﻿using SynonymsTask.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SynonymsTask.Core.Interfaces
{
    public interface ISynonymService
    {
        /// <summary>
        /// Get a list of Terms and associated synonyms
        /// </summary>
        /// <returns></returns>
        List<TermModel> GetTermModelList();

        /// <summary>
        /// Add a new Synonym
        /// </summary>
        /// <param name="sTerm"></param>
        /// <param name="sSynonyms"></param>
        void Add(string sTerm, string sSynonyms);

        /// <summary>
        /// Remove an existing synonym by Id
        /// </summary>
        /// <param name="iSynonymId">Id of the synonym to delete</param>
        void RemoveById(int iSynonymId);


        /// <summary>
        /// Remove an existing synonym by Term (=> possible multi ocurence)
        /// </summary>
        /// <param name="iSynonymId">Id of the synonym to delete</param>
        void RemoveByTerm(string sTerm);

        /// <summary>
        /// Delete all the synonyms
        /// </summary>
        void RemoveAll();
    }
}
